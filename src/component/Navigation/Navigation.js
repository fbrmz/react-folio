import React from 'react';
import styles from './Navigation.module.scss';
import { Link } from 'react-router-dom';

const Navigation = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.wrapperTop}>
        <Link to="/" className={styles.topLeft}>
          Iara Grinspun
        </Link>
        <Link to="/about" className={styles.topRight}>
          About
        </Link>
      </div>
    </div>
  );
};

export default Navigation;
