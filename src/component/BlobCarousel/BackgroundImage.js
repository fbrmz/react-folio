class BackgroundImage {
  constructor(image) {
    this.opacity = 0;
    this.scale = 0;
    this.rotation = 0;
    this.image = image;
  }
}
export default BackgroundImage;
