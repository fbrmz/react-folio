export const getAllChildNodes = node => {
  return [...node].reduce((acc, current) => {
    if ([...current.childNodes].length > 1) {
      return [...acc, ...getAllChildNodes([...current.childNodes])];
    }
    return [...acc, current];
  }, []);
};
