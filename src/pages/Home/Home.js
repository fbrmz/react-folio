import React from 'react';
import styles from './Home.module.scss';
import BlobCarousel from '../../component/BlobCarousel';
import data from '../../data/projects';

const Home = ({ history }) => {
  return (
    <div className={styles.wrapper}>
      <BlobCarousel projects={data.projects} history={history} />
    </div>
  );
};

export default Home;
