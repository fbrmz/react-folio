import React, { useEffect, useRef } from 'react';
import styles from './About.module.scss';
import { useLetterizeLeft } from '../../hooks/useLetterIzeLeft';
import { TweenMax, Expo } from 'gsap';
import ScrollToPlugin from 'gsap/ScrollToPlugin';

const About = () => {
  ScrollToPlugin; // eslint-disable-line
  const titleRef = useRef(null);
  const titleRef2 = useRef(null);
  const wrapperRef = useRef(null);
  useLetterizeLeft(titleRef, 2);
  useLetterizeLeft(titleRef2, 2);
  useEffect(() => {
    TweenMax.to(window, 1, { scrollTo: { y: 0 } });
    document.body.classList.remove('js-overflow-hidden');
  });
  useEffect(
    () => {
      if (wrapperRef.current) {
        TweenMax.fromTo(
          wrapperRef.current,
          1,
          { autoAlpha: 0, y: -100 },
          { autoAlpha: 1, y: 0, Ease: Expo.easeInOut },
        );
      }
    },
    [wrapperRef],
  );
  return (
    <div className={styles.wrapper} ref={wrapperRef}>
      <div className={styles.textAbout}>about</div>
      <div className={styles.heroWrapper}>
        <h1>
          <span ref={titleRef}>Hello,</span>
          <br /> <span ref={titleRef2}>Stalker.</span>
        </h1>
        <span className={styles.years}>2014 - 2020</span>
      </div>
      <div className={styles.gridTop}>
        <div>
          <img src="/img/about.png" alt="About Iara grinspun" />
        </div>
        <div className={styles.twoCols}>
          <span>english</span>
          <div>
            <p>
              I’m Iara Grinspun, a gluten-free passionate visual designer and art director based in
              Buenos Aires, Argentina. I was lucky to found out i loved arts and design since i was
              little, so i ve dedidacted my whole life to it, being able to graduate from the
              University of Buenos Aires with Bachelor’s degree in Graphic Design, specializing in
              Illustration, Editorial Design and UX/UI.
            </p>
            <p>
              {' '}
              I currently work with companies all over the world to help them materialize their
              essence. Till the moment, i’ve worked for agencies such as Dhnn, MediaMonks, Possible
              and currently on Despegar. This experience has provided me the opportunity to develop
              the skills for digitaI design. Im also working as a freelance artist and illustrator.
            </p>
          </div>
        </div>
        <div className={styles.twoCols}>
          <span>spanish</span>
          <div>
            <p>
              Mi nombre es Iara Grinspun, diseñadora gráfica recibida de la Universidad de Buenos, y
              directora de arte. Actualmente estoy viviendo en Buenos Aires, Argentina.{' '}
            </p>
            <p>
              Por el momento estuve trabajando con clientes y empresas de todo el mundo, tales como
              DHNN, Possible, Mediamonks y actualmente, en Despegar.
            </p>{' '}
            <p>
              Estas experiencias me dieron la posibilidad de desarollar mis hablidades como
              diseñadora digital, profundizandome en UX/UI. También me desarollo como freelance
              artist e ilustradora.
            </p>
          </div>
        </div>
      </div>
      <div className={styles.gridBottom}>
        <div className={styles.gridBottomColumn}>
          <h5>clients</h5>
          <ul>
            <li>Microsoft</li>
            <li>Nivea</li>
            <li>Ibm</li>
            <li>Whatsapp</li>
            <li>Adidas</li>
            <li>O.P.I</li>
            <li>Monoblock</li>
            <li>Allegra</li>
            <li>Unilever</li>
            <li>Giraffe games</li>
            <li>Viacom</li>
            <li>Apex America</li>
          </ul>
        </div>
        <div className={styles.gridBottomColumn}>
          <h5>work experience</h5>
          <div className={styles.gridBottomColumnFlex}>
            <ul>
              <li>
                <span>Despegar</span> Visual Designer
              </li>
              <li>
                <span>Possible</span> Art director
              </li>
              <li>
                <span>MediaMonks</span> Digital Graphic Designer
              </li>
              <li>
                <span>Monoblock</span> Graphic Designer & Editorial
              </li>
              <li>
                <span>DHNN</span> Graphic Designer
              </li>
            </ul>
            <ul>
              <li>2019 - today</li>
              <li>2019</li>
              <li>2018</li>
              <li>2017</li>
              <li>2015-2017</li>
            </ul>
          </div>
        </div>
        <div className={styles.gridBottomColumn}>
          <h5>skills</h5>
          <ul>
            <li>Art direction</li>
            <li>Brand concept</li>
            <li>Identity system</li>
            <li>Brand guidelines</li>
            <li>Editorial design</li>
            <li>Brochure</li>
            <li>Art cover</li>
            <li>Magazines</li>
            <li>Editorial systems</li>
            <li>Editorial website</li>
            <li>Ux Design</li>
          </ul>
        </div>
        <div className={styles.gridBottomColumn}>
          <h5>languages</h5>
          <ul>
            <li>English</li>
            <li>Spanish</li>
          </ul>
          <h5>Contact</h5>
          <ul>
            <li>
              <a href="https://www.behance.net/iaragrinspun" target="_blank">
                Behance
              </a>
            </li>
            <li>
              <a href="https://dribbble.com/iaragrinspunn" target="_blank">
                Dribbble
              </a>
            </li>
            <li>
              <a href="https://www.linkedin.com/in/iara-grinspun-17647478" target="_blank">
                LinkedIn
              </a>
            </li>
          </ul>
        </div>
        <div className={styles.gridBottomColumn}>
          <h5>Publications</h5>
          <ul>
            <li>
              <a href="" target="_blank">
                Capstules Book Interview
              </a>
            </li>
            <li>
              <a href="" target="_blank">
                Filtro featured on University website
              </a>
            </li>
            <li>
              <a href="" target="_blank">
                Flipping pages publication
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className={styles.bottomContent}>
        <h5>
          TO ORDER GLUTEN - FREE FOOD
          <span>
            <svg width="390px" height="30px" viewBox="0 0 390 30" version="1.1">
              <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="About" transform="translate(-635.000000, -2794.000000)">
                  <g
                    id="arrow"
                    transform="translate(830.000000, 2809.000000) scale(-1, 1) rotate(90.000000) translate(-830.000000, -2809.000000) translate(815.000000, 2614.000000)"
                    fill="#000000"
                  >
                    <polygon
                      id="Fill-154"
                      points="30 376.416834 15.000272 390 0 376.416834 1.43478971 375.117545 13.980086 386.478074 13.980086 0 16.0204581 0 16.0204581 386.478074 28.5652103 375.117545"
                    />
                  </g>
                </g>
              </g>
            </svg>
          </span>
        </h5>
        <p>
          Email me at:{' '}
          <a href="mailto:iaragrinspun@gmail.com" target="_blank">
            iaragrinspun@gmail.com
          </a>
        </p>
      </div>
    </div>
  );
};

export default About;
