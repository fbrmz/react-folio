import React from 'react';
import styles from './{{name_pc}}.module.scss';

const {{name_pc}} = () => <div className={styles.wrapper}>{{name_pc}}</div>;

export default {{name_pc}};
